import { useState } from 'react';
import TodoForm from './TodoForm';
import Todo from './Todo';

const TodoList = () => {
	const [todos, setTodos] = useState([]);

	// ADD NOVO TODO
	const addTodo = (todo) => {
		if (!todo.text) {
			return;
		}
		const newTodos = [todo, ...todos];
		setTodos(newTodos);
	};

	// REMOVE UM TODO
	const removeTodo = (id) => {
		const removeItem = [...todos].filter((todo) => todo.id !== id);
		setTodos(removeItem);
	};

	//COMPLETA UM TODO
	const completeTodo = (id) => {
		const updatedTodos = todos.map((todo) => {
			if (todo.id == id) {
				todo.isComplete = !todo.isCompĺete;
			}
			return todo;
		});
		setTodos(updatedTodos);
	};

	return (
		<div>
			<TodoForm onSubmit={addTodo} />
			<Todo
				todos={todos}
				completeTodo={completeTodo}
				removeTodo={removeTodo}
			/>
		</div>
	);
};

export default TodoList;
