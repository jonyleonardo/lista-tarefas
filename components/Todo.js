import { useState } from 'react';
import TodoForm from './TodoForm';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt, faCheckCircle } from '@fortawesome/free-solid-svg-icons';

const Todo = ({ todos, completeTodo, removeTodo }) => {
	const [edit, setEdit] = useState({
		id: null,
		value: '',
	});

	return todos.map((todo, index) => (
		//  LISTA DE TODOS
		// OBS: CLASSE DINÂMICA P/ TODO E P/ TODO COMPLETO
		<div key={index} className={todo.isComplete ? 'todoComplete' : 'todo'}>
			<div key={todo.id}>
				{todo.text}

				<div className="icons">
					<FontAwesomeIcon
						className="icon1"
						icon={faCheckCircle}
						onClick={() => completeTodo(todo.id)}
						className="edit"
					/>
				</div>
				<div className="icons">
					<FontAwesomeIcon
						icon={faTrashAlt}
						onClick={() => removeTodo(todo.id)}
						className="delete"
					/>
				</div>
			</div>
		</div>
	));
};
export default Todo;
