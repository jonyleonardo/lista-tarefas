import { useState } from 'react';
import { Row, Col, Container, Button, Form } from 'react-bootstrap';

const TodoForm = (props) => {
	const [input, setInput] = useState('');

	// Faz o two-way-data-binding
	const change = (event) => {
		return setInput(event.target.value);
	};

	// Função SUBMIT
	const submit = (event) => {
		event.preventDefault();

		props.onSubmit({
			id: Math.floor(Math.random() * 10),
			text: input,
		});

		setInput('');
	};

	return (
		<Container>
			<Row>
				<Col xs={12} sm={12} md={12} lg={12}>
					<Form className="mt-4" onSubmit={submit}>
						<Form.Group controlId="formBasicEmail">
							<div className="inputTodo">
								<Form.Label className="ml-2 button">
									Digite a tarefa
								</Form.Label>
								<Form.Control
									type="text"
									placeholder="Nova Tarefa"
									className="inputForm"
									value={input}
									onChange={change}
								/>
								<Form.Text className="text-muted ml-2 button">
									Digite uma tarefa à ser concluida
								</Form.Text>
							</div>
							<div>
								<Button
									className="button mt-3 buttonForm"
									variant="warning"
									type="submit"
								>
									Adicionar
								</Button>
							</div>
						</Form.Group>
					</Form>
				</Col>
			</Row>
		</Container>
	);
};

export default TodoForm;
