import { Container } from 'react-bootstrap';
import TodoList from '../components/TodoList';

export default function Home(props) {
	return (
		<Container className="containerPrincipal">
			<TodoList />
		</Container>
	);
}
