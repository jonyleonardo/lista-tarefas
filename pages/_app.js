import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../components/Form.css';
import '../components/Todo.css';
function MyApp({ Component, pageProps }) {
	return <Component {...pageProps} />;
}

export default MyApp;
